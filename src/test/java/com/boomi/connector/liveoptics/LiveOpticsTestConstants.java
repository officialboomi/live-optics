//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.liveoptics;
/**
 * @author aditi ardhapure
 *
 * ${tags}
 */
public class LiveOpticsTestConstants {
	public static final String URL_PROPERTY = "url";
	public static final String DEFAULT_URL = "https://papi.liveoptics.com";
	public static final String SHARED_SECRET = "+idBbAUqzNVV02z300zD5PLOfuABOuTWypBv4s2xEqwf";
	public static final String LOGIN_SECRET = "0BSSbaAxya5Cwbmjm/s4y7Kv5ua2pvuDM228s5dc+efp";
	public static final String LOGIN_TOKEN = "LoginToken";
	public static final String INCLUDE_ENTITIES = "true";
	public static final String LOGIN_ID_1 = "Z9DrRp6NCdX45HJY0X+5hY3VdJkYO2793/wUPd8S4T6o";
	public static final String MAYBESESSION = "1|20191127035005hFS4mo7rT7jsrY9Aj8JtWg==aAvM5S5TnOadP8Vg8Tq7KfQj9J6Ppj2yqPYLN6+gzqXziUeh6sERzzhWIi95Lw1SG4dSExWOIfunDLY5HCOu1QfUR7y1rO4vO9W4lh/Yl9DtL4D+Mmi6fjZQjg9bcK09DjxZpyeS0WEHCiqdO5tNFsBwCssZioIVUgPegve6JYx/88vriA/UUcpgWwBAO4Vk7T3i1y0MvZsZfnub7S26ug==|/lKq43Zt/RMlYWTwepjkXVs2f+yHJQnlYKUlrTQXxImNRuqzfoimGBKlP4Ohkl7HnmUw5Vhw8KrLqKU+HW4SOA==";
	public static final String MAYBESESSION_LENGTHCHECK = "1|20191127035005hFS4mo7rT7jsrY9Aj8JtWg==aAvM5S5TnOadP8Vg8Tq7KfQj9J6Ppj2yqPYLN6+gzqXziUeh6sERzzhWIi95Lw1SG4dSExWOIfunDLY5HCOu1QfUR7y1rO4vO9W4lh/Yl9DtL4D+Mmi6fjZQjg9bcK09DjxZpyeS0WEHCiqdO5tNFsBwCssZioIVUgPegve6JYx/88vriA/UUcpgWwBAO4Vk7T3i1y0MvZsZfnub7S26ug==|/lKq43Zt/RMlYWTwepjkXVs2f+yHJQnlYKUlrTQXxImNRuqzfoimGBKlP4Ohkl7HnmUw5|hw8KrLqKU+HW4S|A==";
	public static final String MAYBESESSION_BLOBCHECK = "1|0191127035005hFS4mo7rT7jsrY9Aj8JtWg==aAvM5S5TnOadP8Vg8Tq7KfQj9J6Ppj2yqPYLN6+gzqXziUeh6sERzzhWIi95Lw1SG4dSExWOIfunDLY5HCOu1QfUR7y1rO4vO9W4lh/Yl9DtL4D+Mmi6fjZQjg9bcK09DjxZpyeS0WEHCiqdO5tNFsBwCssZioIVUgPegve6JYx/88vriA/UUcpgWwBAO4Vk7T3i1y0MvZsZfnub7S26ug==|u/lKq43Zt/RMlYWTwepjkXVs2f+yHJQnlYKUlrTQXxImNRuqzfoimGBKlP4Ohkl7HnmUw5Vhw8KrLqKU+HW4SOA==";
	public static final String MAYBESESSION_LESSERLENGTH = "11|201911";
	public static final String MAYBESESSION_HMACTEST = "1|20191127035005hFS4mo7rT7jsrY9Aj8JtWg==aAvM5S5TnOadP8Vg8Tq7KfQj9J6Ppj2yqPYLN6+gzqXziUeh6sERzzhWIi95Lw1SG4dSExWOIfunDLY5HCOu1QfUR7y1rO4vO9W4lh/Yl9DtL4D+Mmi6fjZQjg9bcK09DjxZpyeS0WEHCiqdO5tNFsBwCssZioIVUgPegve6JYx/88vriA/UUcpgWwBAO4Vk7T3i1y0MvZsZfnub7S26ug==|/lKq43Zt/RMlYWTwepjkXVs2f+yHJQnlYKUlrTQXxImNRuqzfoimGBKlP4Ohkl7HnmUw5Vhw8KrLqKU+HW4SOA==";
	public static final String MAYBESESSION_VERSIONLENGTH = "313|0191127035005hFS4mo7rT7jsrY9Aj8JtWg==aAvM5S5TnOadP8Vg8Tq7KfQj9J6Ppj2yqPYLN6+gzqXziUeh6sERzzhWIi95Lw1SG4dSExWOIfunDLY5HCOu1QfUR7y1rO4vO9W4lh/Yl9DtL4D+Mmi6fjZQjg9bcK09DjxZpyeS0WEHCiqdO5tNFsBwCssZioIVUgPegve6JYx/88vriA/UUcpgWwBAO4Vk7T3i1y0MvZsZfnub7S26ug==|lKq43Zt/RMlYWTwepjkXVs2f+yHJQnlYKUlrTQXxImNRuqzfoimGBKlP4Ohkl7HnmUw5Vhw8KrLqKU+HW4SOA==";
	public static final String SESSIONTOKEN = "1|201911270817544Sf3-gz1XGTTydnAp785DQz6AMsGdimC9nmwZTYC5we8p3cH_Q4hXbLUkZyH4IPDkkfhk2ylarNswqOw_YUhAzbTsRTIYuMYfe5dBKjcCHXwLVMYSRRtoYZUJajqis52Rg";
	public static final String JSONSCHEMAURL = "https://papi.liveoptics.com/swagger/docs/v1";
}