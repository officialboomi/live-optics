//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.liveoptics;

/**
 * @author Sudeshna Bhattacharjee
 *
 * ${tags}
 */

@SuppressWarnings("serial")
public class ArgumentNullException extends Exception {
         public ArgumentNullException(String s) {
        	 super(s);
         }
}
